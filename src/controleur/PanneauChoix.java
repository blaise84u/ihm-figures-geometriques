package controleur;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import modele.*;
import modele.Rectangle;
import vue.VueDessin;

@SuppressWarnings("serial")
public class PanneauChoix extends JPanel {

	private VueDessin vdessin;
	private DessinModele dmodele;
	private FigureColoree fc;
	
	final JComboBox<String> type_fig;
	final JComboBox<String> couleurs;
	
	
	
/*
 * Constructeur de PanneauChoix
 */
	public PanneauChoix(VueDessin vdessin) {
		
		this.vdessin = vdessin;
		this.setLayout(new BorderLayout());
		
		dmodele = new DessinModele();
		dmodele.addObserver(this.vdessin);
		dmodele.initDessinModele();

	
		

		final JRadioButton b1 = new JRadioButton("Nouvelle figure");
		final JRadioButton b2 = new JRadioButton("Trac� � main lev�e ");
		final JRadioButton b3 = new JRadioButton("Manipulations");
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(b1);
		bg.add(b2);
		bg.add(b3);
		
		JPanel haut = new JPanel();
		haut.add(b1);
		haut.add(b2);
		haut.add(b3);
		add(haut, BorderLayout.NORTH);
		
		
		b1.doClick();
		b1.setSelected(true);
		
		type_fig = new JComboBox<String>(new String[] { "quadrilat�re",
				"triangle", "rectangle", "cercle" });
		couleurs = new JComboBox<String>(new String[] { "rouge", "verte",
				"bleu", "jaune","noir","gris","orange","rose"});
		
		type_fig.setSelectedIndex(0);
		couleurs.setSelectedIndex(0);
		
		fc=creeFigure(type_fig.getSelectedIndex());		
		fc.changeCouleur(determineCouleur(couleurs.getSelectedIndex()));
		vdessin.construit(fc);
		type_fig.setEnabled(true);
		
		
		couleurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = determineCouleur(couleurs.getSelectedIndex());
				if((b1.isSelected())&& (fc != null)) {					
					fc.changeCouleur(c);
					dmodele.majAffichage();
				}
				if(b2.isSelected()) {
					vdessin.supprimeAuditeurs();
					vdessin.trace(false,c);
				}
				if(b3.isSelected()) {
					if(vdessin.figureSelection()!=null){
						fc=vdessin.figureSelection();
						dmodele.changeCoul(fc,c);
						dmodele.majAffichage();
					}
				}

			}
		});
		
		
		
		type_fig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(type_fig.getSelectedIndex()!=0){	
					fc = creeFigure(type_fig.getSelectedIndex());
					fc.changeCouleur(determineCouleur(couleurs.getSelectedIndex()));
					vdessin.supprimeAuditeurs();
					vdessin.construit(fc);
				}else{
					fc = new Quadrilatere();
					fc.changeCouleur(determineCouleur(couleurs.getSelectedIndex()));
					vdessin.supprimeAuditeurs();
					vdessin.construit(fc);
				}

			}

		});	
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fc=creeFigure(type_fig.getSelectedIndex());		
				fc.changeCouleur(determineCouleur(couleurs.getSelectedIndex()));
				vdessin.construit(fc);
				vdessin.desactiverManipulationsSouris();
				type_fig.setEnabled(true);			
				}
		});
		
		
		
		
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				type_fig.setEnabled(false);
				vdessin.supprimeAuditeurs();
				vdessin.trace(true,determineCouleur(couleurs.getSelectedIndex()));
			}
		});
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(dmodele.getSel()!=-1)
				fc = dmodele.getLfg().get(dmodele.getSel());
				type_fig.setEnabled(false);
				
				vdessin.supprimeAuditeurs();
				vdessin.activeManipulationsSouris();
			}
		});

		
		
		
		JPanel bas = new JPanel();
		bas.add(type_fig);
		bas.add(couleurs);
		add(bas, BorderLayout.SOUTH);
		
		haut.setBackground(Color.gray);
		bas.setBackground(Color.gray);

	}
	
	/*
	 * Determine la couleur
	 * @param i
	 * 			L'indice de la JComboBox
	 * @return Color
	 *			La couleur choisit
	 */

	private Color determineCouleur(int i) {
		switch (i) {
		case 0:
			return Color.RED;
		case 1:
			return Color.green;
		case 2:
			return Color.blue;
		case 3:
			return Color.yellow;
	
		case 4:
			return Color.black;
			
		case 5:
			return Color.gray;
			
		case 6:
			return Color.orange;
			
		case 7:
			return Color.pink;
		}
		return null;

	}
	/*
	 * Determine la couleur
	 * @param i
	 * 			L'indice de la JComboBox
	 * @return fc
	 *			La FigureColoree construit
	 */

	private FigureColoree creeFigure(int i) {
		FigureColoree fc = null;
		switch (i) {
		case 0:
			fc = new Quadrilatere();
			break;
		case 1:
			fc = new Triangle();
			break;
		case 2:
			fc = new Rectangle();
			break;
		case 3:
			fc = new Cercle();
			break;
		}
		return fc;
	}
}
