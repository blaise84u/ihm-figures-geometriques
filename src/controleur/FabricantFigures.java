package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import modele.DessinModele;
import modele.FigureColoree;
import modele.Point;
import vue.VueDessin;

public class FabricantFigures implements MouseListener {

	private FigureColoree figure_en_cours_de_fabrication;
	private int nb_points_cliques;
	private Point[] points_cliques;
	
	/*
	 * Constructeur de FabricantFigures
	 */
	public FabricantFigures(FigureColoree fc) {
		if (fc != null) {
			figure_en_cours_de_fabrication = fc;
			nb_points_cliques = 0;
			points_cliques = new Point[fc.nbClics()];
		}
	}

	public void mousePressed(MouseEvent e) {
		if (figure_en_cours_de_fabrication != null) {
			if (nb_points_cliques < points_cliques.length - 1) {
				points_cliques[nb_points_cliques] = new Point(e.getX(),
						e.getY());
				nb_points_cliques++;
			} else {
				points_cliques[nb_points_cliques] = new Point(e.getX(),
						e.getY());
				figure_en_cours_de_fabrication.modifierPoints(points_cliques);
				DessinModele dm = (((VueDessin) (e.getSource())).getDessin());
				dm.ajoute(figure_en_cours_de_fabrication);
				((VueDessin) (e.getSource())).removeMouseListener(this);
			}
			

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}
