package controleur;

import java.awt.Color;

public class Trait {
	
	private int debx,deby,finx,finy;
	private Color couleur;
	
	/*
	 * Constructeur de Trait
	 */
	public Trait(int debx, int deby, int finx, int finy, Color couleur) {
		this.debx = debx;
		this.deby = deby;
		this.finx = finx;
		this.finy = finy;
		this.couleur = couleur;
	}
	public int getDebx() {
		return debx;
	}
	public void setDebx(int debx) {
		this.debx = debx;
	}
	public int getDeby() {
		return deby;
	}
	public void setDeby(int deby) {
		this.deby = deby;
	}
	public int getFinx() {
		return finx;
	}
	public void setFinx(int finx) {
		this.finx = finx;
	}
	public int getFiny() {
		return finy;
	}
	public void setFiny(int finy) {
		this.finy = finy;
	}
	public Color getCouleur() {
		return couleur;
	}
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

}
