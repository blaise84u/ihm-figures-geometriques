package controleur;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import modele.DessinModele;


public class TraceurForme extends JPanel implements MouseMotionListener,MouseListener {
	
	private int last_x,last_y;
	private Color couleur_trait;
	private Graphics gc;
	private DessinModele dm;
	
	private ArrayList<Trait> tableau_traits;
	
	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
	
	/*
	 * Constructeur de TraceurForme
	 */
	public TraceurForme(Graphics gc,DessinModele dm){
		this.couleur_trait = Color.black;
		this.gc=gc;
		this.dm=dm;
		tableau_traits=new ArrayList<Trait>();
		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			this.last_x=e.getX();
			this.last_y=e.getY();
		}
		if (SwingUtilities.isMiddleMouseButton(e)) {
			tableau_traits.clear();
			((JComponent) (e.getSource())).repaint();	
		}
		
	}
	
	@Override
	public void mouseDragged(MouseEvent e) { 
		
		Trait trait=new Trait(this.last_x, this.last_y,e.getX(), e.getY(), this.couleur_trait);
		for(Trait t:tableau_traits){
			gc.setColor(t.getCouleur());
			gc.drawLine(t.getDebx(), t.getDeby(), t.getFinx(), t.getFiny() );
		}
		this.last_x = e.getX();
		this.last_y = e.getY();
		tableau_traits.add(trait);
		dm.majAffichage();
	
	}
	
	public void setColor(Color c){
		this.couleur_trait=c;
	}
	
	public ArrayList<Trait> getTableau_traits(){
		return tableau_traits;
	}

}
