package controleur;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import modele.*;

public class ManipulateurFormes implements MouseListener, MouseMotionListener {

	private int last_x, last_y;

	private DessinModele dm;

	private ArrayList<FigureColoree> lfg;

	private boolean trans;

	private int nbf, sel, indice;

	public ManipulateurFormes(DessinModele dm) {
		this.dm = dm;
		this.lfg = dm.getLfg();
		this.nbf = dm.getNbf();
		this.sel = dm.getSel();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		if (sel != -1) {
			lfg.get(sel).deSelectionne();
			dm.majAffichage();
		}
		sel = -1;
		last_x = e.getX();
		last_y = e.getY();
		for (int i = nbf - 1; i >= 0; i--) {
			if (lfg.get(i).estDedans(last_x, last_y)) {
				sel = i;
				lfg.get(sel).selectionne();
				dm.majAffichage();
				if (SwingUtilities.isRightMouseButton(e)) {
					indice = lfg.get(i).carreDeSelection(last_x, last_y);
					trans = (indice != -1);
				}
				break;
			} else {
				if (SwingUtilities.isRightMouseButton(e)) {
					if (lfg.get(i).carreDeSelection(last_x, last_y) != -1) {
						indice = lfg.get(i).carreDeSelection(last_x, last_y);
						trans = (indice != -1);
						sel = i;
						break;
					}

				}
			}

		}
		if (sel != -1) {
			FigureColoree f = lfg.get(sel);
			lfg.set(sel, lfg.get(nbf - 1));
			lfg.set(nbf - 1, f);
			sel = nbf - 1;
		}

	}

	public void mouseDragged(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			if (sel >= 0) {
				lfg.get(sel).translation(e.getX() - last_x, e.getY() - last_y);
				last_x = e.getX();
				last_y = e.getY();
				dm.majAffichage();
			}
		} else {
			if (SwingUtilities.isRightMouseButton(e)) {
				if (sel >= 0) {
					if (trans) {
						lfg.get(sel).transformation(indice, e.getX() - last_x, e.getY() - last_y);
						last_x = e.getX();
						last_y = e.getY();
						dm.majAffichage();
					}
				}
			}
		}
	}

	public int nbFigures() {
		return this.nbf;
	}

	public FigureColoree figureSelection() {
		ArrayList<FigureColoree> l = dm.getLfg();
		dm.setSel(this.sel);
		if(this.sel==-1){
			return null;
		}
		return l.get(sel);

	}
}
