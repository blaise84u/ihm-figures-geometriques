package main;

import java.awt.*;

import javax.swing.*;

import controleur.PanneauChoix;
import vue.VueDessin;

@SuppressWarnings("serial")
public class Fenetre extends JFrame {

	VueDessin vdessin;
	PanneauChoix contenantChoix;

	/**
	 * Constructeur de Fenetre
	 */

	public Fenetre(String s, int w, int h) {
		super(s);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		vdessin = new VueDessin();
		JPanel contenant = new JPanel();
		contenant.setLayout(new BorderLayout());

		contenantChoix = new PanneauChoix(vdessin);
		contenant.add(contenantChoix, BorderLayout.NORTH);
		contenant.add(vdessin, BorderLayout.CENTER);

		this.setContentPane(contenant);
		this.setPreferredSize(new Dimension(w, h));
		this.pack();
		
	}

	public static void main(String[] args) {
		Fenetre f = new Fenetre("Figures Geometriques", 800, 600);
		f.setVisible(true);
	}
}