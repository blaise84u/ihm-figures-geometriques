package vue;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import controleur.*;
import modele.*;


@SuppressWarnings("serial")
public class VueDessin extends JPanel implements Observer {
	private DessinModele dessin;
	private ManipulateurFormes mf;
	private TraceurForme tf;
	private ArrayList<Trait> tableau_traits;
	

	@Override
	public void update(Observable o, Object arg) {
		dessin = (DessinModele) o;
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (dessin != null) {
			for (FigureColoree fc : dessin.getLfg()) {
				fc.affiche(g);
			}
		}
		if(tf!=null){
			tableau_traits=tf.getTableau_traits();
			for(Trait t:tableau_traits){
				g.setColor(t.getCouleur());
				g.drawLine(t.getDebx(), t.getDeby(), t.getFinx(), t.getFiny() );
				
			}
		}
	}

	public VueDessin() {
		this.dessin=null;
	}

	public void construit(FigureColoree fc) {
		if (fc != null) {
			this.addMouseListener(new FabricantFigures(fc));
		}
	}

	public DessinModele getDessin() {
		return this.dessin;
	}

	public void supprimeAuditeurs() {
		MouseListener [] ml=(MouseListener[])(getListeners(MouseListener.class));
		for (int i=0;i<ml.length;i++)
			removeMouseListener(ml[i]);
		MouseMotionListener[] mml=(MouseMotionListener[])(getListeners(MouseMotionListener.class));
		for(int i=0;i<mml.length;i++)
			removeMouseMotionListener(mml[i]);
		
	}
	
	public void activeManipulationsSouris() {
		mf=new ManipulateurFormes(dessin);
		this.addMouseListener(mf); 
		this.addMouseMotionListener(mf);
	}
	
	
	 public void desactiverManipulationsSouris(){
		 if(mf!=null){
		 this.removeMouseListener(mf);
		 this.removeMouseMotionListener(mf);
		 }
		 if(tf!=null){
			 this.removeMouseListener(tf);
			 this.removeMouseMotionListener(tf);
		 }
		 mf=null;
		 
	 }
	 
	 public FigureColoree figureSelection(){
		 mf.figureSelection();
		 int sel=dessin.getSel();
		 if(sel==-1){
			 return null;
		 }
		 return dessin.getLfg().get(sel);
	 
	 }

	public void trace(boolean b,Color c) {
		if(b&&tf==null){
		tf=new TraceurForme(this.getGraphics(),dessin);
		}
		this.addMouseListener(tf); 
		this.addMouseMotionListener(tf);
		tf.setColor(c);
		
	}
}
