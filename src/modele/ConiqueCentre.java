package modele;

public abstract class ConiqueCentre extends FigureColoree {

	protected Point centre;

	public ConiqueCentre() {
		super();
	}
	
	public Point rendreCentre(){
		return centre;
	}

	
	
}
