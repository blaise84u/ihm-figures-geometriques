package modele;

import java.awt.*;
import java.awt.Rectangle;

public abstract class FigureColoree {
	
	static final private int TAILLE_CARRE_SELECTION=6;
	
	protected Point[] tab_mem;
	
	private boolean selected;
	
	protected Color couleur;
	
	
	public FigureColoree(){
		tab_mem=new Point[this.nbPoints()];
		selected=false;
		couleur=Color.BLUE;
		
	}
	
	public abstract int nbPoints();
	
	public abstract int nbClics();
	
	public abstract void modifierPoints(Point [] ps);
	
	public void affiche(Graphics g){
		if (selected) {
			g.setColor(Color.BLACK);
			for(int i=0;i<tab_mem.length;i++){
				g.fillRect(tab_mem[i].rendreX() - TAILLE_CARRE_SELECTION/2,
						   tab_mem[i].rendreY() - TAILLE_CARRE_SELECTION/2,
						   TAILLE_CARRE_SELECTION,
						   TAILLE_CARRE_SELECTION);
			
			}
			g.setColor(couleur);
		}
	}

	public void selectionne() {
		selected=true;
	}
	
	public void deSelectionne(){
		selected=false;

	}
	
	public void changeCouleur(Color c){
		couleur=c;
	}
	
	public void translation(int dx, int dy){
		for(int i=0;i<tab_mem.length;i++){
			tab_mem[i].translation(dx,dy);
		}
	}

	public int carreDeSelection(int last_x, int last_y) {
		Rectangle carre=null;
		int j=-1;
		for(int i=0;i<tab_mem.length;i++){
			carre=new Rectangle(tab_mem[i].rendreX() - TAILLE_CARRE_SELECTION/2,
					   tab_mem[i].rendreY() - TAILLE_CARRE_SELECTION/2,
					   TAILLE_CARRE_SELECTION,
					   TAILLE_CARRE_SELECTION);
			if(carre.contains(last_x,last_y)){
				j=i;
				break;
			}
		}
		return j;
	}

	public abstract boolean estDedans(int last_x, int last_y);

	public void transformation(int indice,int i, int j) {
		Point p=tab_mem[indice];
		p.translation(i, j);
		tab_mem[indice]=p;
		this.selectionne();
	}



	

}
