package modele;

public class Point {
	
	private int x,y;
	
	public Point(int abs,int ord){
		this.x=abs;
		this.y=ord;
	}
	
	public double distance(Point p){
		int  distanceX=p.rendreX()-this.x;
		int distanceY=p.rendreY()-this.y;
		return Math.sqrt(distanceX*distanceX+distanceY*distanceY); 
	}
	
	public void incrementerX(int incx){
		this.x += incx;
		
	}
	
	public void incrementerY(int incY){
		this.x += incY;
		
	}
	
	public int rendreX(){
		return x;
	}
	
	public int rendreY(){
		return y;
	}
	
	public void modifierX(int x){
		this.x=x;
	}
		
	public void modifierY(int y){
		this.y=y;
	}
	
	public void translation(int dx,int dy){
		x+=dx;
		y+=dy;
	}
}
	

