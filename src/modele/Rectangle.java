package modele;

public class Rectangle extends Quadrilatere {
	
	public Rectangle(){
		super();
	}
	
	public int nbClics(){
		return 2;
	}
	
	public void modifierPoints(Point[] tab_saisie){
		if(tab_saisie.length==2) {
			tab_mem[0]=tab_saisie[0];
			tab_mem[2]=tab_saisie[1];
			tab_mem[1]=new Point(tab_mem[2].rendreX(), tab_mem[0].rendreY());
			tab_mem[3]=new Point(tab_mem[0].rendreX(), tab_mem[2].rendreY());
		}
		
	}
	
	public void transformation(int indice,int i,int j){	
		Point p=tab_mem[indice];
		p.translation(i, j);
		switch(indice){
		case 0:		
			tab_mem[3].modifierX(p.rendreX());
			tab_mem[1].modifierY(p.rendreY());
			break;
		case 1:
			tab_mem[2].modifierX(p.rendreX());
			tab_mem[0].modifierY(p.rendreY());
			break;
		case 2:
			tab_mem[1].modifierX(p.rendreX());
			tab_mem[3].modifierY(p.rendreY());
			break;
		case 3:
			tab_mem[0].modifierX(p.rendreX());
			tab_mem[2].modifierY(p.rendreY());
			break;
		}
		tab_mem[indice]=p;
		this.selectionne();
			
	}

}
