package modele;

import java.awt.Graphics;
import java.awt.Polygon;

public class Cercle extends ConiqueCentre {
	
	private double rayon;
	
	public double rendreRayon() {
		return rayon;
	}

	public Cercle() {
		super();
	}

	@Override
	public int nbPoints() {
		return 2;
	}

	@Override
	public int nbClics() {
		return 2;
	}

	@Override
	public void modifierPoints(Point[] ps) {
		if(ps!=null){
		 	if(ps.length==nbPoints()){
		 		tab_mem=ps;
		 	}
		}
	}

	@Override
	public boolean estDedans(int last_x, int last_y) {
		Point p=this.rendreCentre();
		if(((last_x-p.rendreX())*(last_x-p.rendreX())+(last_y-p.rendreY())*(last_y-p.rendreY()))>this.rayon*this.rayon){
			return false;
		}else{
			return true;
		}
	}
	public void affiche(Graphics g){
		rayon=(tab_mem[1].rendreX()-tab_mem[0].rendreX())*(tab_mem[1].rendreX()-tab_mem[0].rendreX())+(tab_mem[0].rendreY()-tab_mem[1].rendreY())*(tab_mem[0].rendreY()-tab_mem[1].rendreY());
		rayon=Math.sqrt(rayon)/2;
		centre=new Point((tab_mem[0].rendreX()+tab_mem[1].rendreX())/2,(tab_mem[0].rendreY()+tab_mem[1].rendreY())/2);
		g.setColor(couleur);
		g.fillOval((int)((tab_mem[0].rendreX()+tab_mem[1].rendreX())/2-rayon),(int)((tab_mem[0].rendreY()+tab_mem[1].rendreY())/2-rayon),(int)(rayon*2),(int)(rayon*2));
 		super.affiche(g);
	}
	
	

	
	public void transformation(int indice,int i,int j){
		Point p=tab_mem[indice];
		p.translation(i, j);
		tab_mem[indice]=p;
		this.selectionne();
	}

}
