package modele;

import java.awt.*;
import java.util.*;

public class DessinModele extends Observable {
	
	private ArrayList<FigureColoree> lfg;
	private int sel,nbf;
	
	public int getNbf() {
		this.nbf=lfg.size();
		return this.nbf;
	}
	
	public void setNbf(int i){
		this.nbf+=i;
	}

	public int getSel() {
		return this.sel;
	}
	
	public void setSel(int i){
		this.sel=i;
	}
	
	public ArrayList<FigureColoree> getLfg() {
		return lfg;
	}
	
	public DessinModele(){
		this.sel=-1;
		
	}
	
	public void initDessinModele()
	{
		this.lfg= new ArrayList<FigureColoree>();
		this.sel=-1;
		setChanged();
		notifyObservers();
	}
	
	public void ajoute(FigureColoree fc){
		if(fc!=null) {
			lfg.add(fc);
			this.sel++;
			if(this.sel!= 0) 
				{
				lfg.get(this.sel-1).deSelectionne();
				
			fc.selectionne();
			}else{
				fc.selectionne();
			}
		}
		setChanged();
		notifyObservers();
	}
	
	public void changeCoul(FigureColoree fc, Color c){
		fc.changeCouleur(c);
		lfg.set(sel, fc);

	}
	
	public void majAffichage(){
		setChanged();
		notifyObservers();
	}


	
	
	

}
